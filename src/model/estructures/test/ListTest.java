package model.estructures.test;

import junit.framework.TestCase;
import model.data_structures.List;
import model.data_structures.Node;
import model.data_structures.SortingAlgorithms;

public class ListTest extends TestCase {
	
	private List lista;
	private List lista1;
	
	/** 
	 * Crea una lista con cinco elementos 
	 */
	public void setupEscenario1()
	{
		lista = new List<Integer>();
		lista.add(1);
		lista.add(2);
		lista.add(4);
		lista.add(3);
		lista.add(5);
		
	}
	
	public void setupEscenario2()
	{
		lista = new List<Integer>();
		lista.add(3);
		lista.add(4);
		lista.add(1);
		lista.add(2);
		lista.add(5);
	}
	
	public void setupEscenario3()
	{
		lista = new List<Integer>();
		lista.add(4);
		lista.add(8);
		lista.add(9);
		lista.add(10);
		lista.add(7);
		lista.add(1); 
		lista.add(12);
		lista.add(3);
		lista.add(6);
		lista.add(13);
		lista.add(15);
		lista.add(5);
		
	}
	
	public void setupEscenario4()
	{
		lista = new List<Integer>();
		
		lista.add(1);
		lista.add(2);
		lista.add(3);

	}
	
	public void setupEscenario5()
	{
		lista = new List<Integer>();
	}
	
	public void setupEscenario6()
	{
		lista = new List<Integer>();
		lista.add(1);
		lista.add(2);
		
	}
	
	
	public void setupEscenario7()
	{
		lista = new List<Integer>();
		lista.add(3);
		lista.add(2);
		lista.add(1);
		
		
		
		
		
	}
	
	public void testRemoveFirst()
	{
		setupEscenario4();
		lista.removeFirst();
		assertEquals("No se elimino el primero", 2, lista.getFirst().getObject());
		lista.removeFirst();

		lista.removeFirst();
	}
	
	public void testLast()
	{
		setupEscenario4();
		lista.removeFirst();
		assertEquals("El ultimo no es correcto", 3, lista.getLast().getObject());
		lista.removeFirst();
		lista.removeFirst();
		assertEquals("El ultimo no es correcto", null, lista.getLast());
	}
	
	public void testGetPositionInt()
	{
		setupEscenario1();
		
		assertEquals("La posicion no es la correcta", 1, lista.get(0));
		assertEquals("La posicion no es la correcta", 2, lista.get(1));
		assertEquals("La posicion no es la correcta", 4, lista.get(2));
		assertEquals("La posicion no es la correcta", 3, lista.get(3));
		assertEquals("La posicion no es la correcta", 5, lista.get(4));
		
	}
	
	public void testExchange()
	{
		setupEscenario1();
		lista.exchange(2, 3);
		assertEquals("La posicion no es la correcta", 1, lista.get(0));
		assertEquals("La posicion no es la correcta", 2, lista.get(1));
		assertEquals("La posicion no es la correcta", 3, lista.get(2));
		assertEquals("La posicion no es la correcta", 4, lista.get(3));
		assertEquals("La posicion no es la correcta", 5, lista.get(4));
	}
	
	public void testExchange2()
	{
		setupEscenario2();
		
		lista.exchange(0, 2 );
		lista.exchange(1, 3);
		assertEquals("La posicion no es la correcta", 1, lista.get(0));
		assertEquals("La posicion no es la correcta", 2, lista.get(1));
		assertEquals("La posicion no es la correcta", 3, lista.get(2));
		assertEquals("La posicion no es la correcta", 4, lista.get(3));
		assertEquals("La posicion no es la correcta", 5, lista.get(4));
	}
	
	
	
	public void testMergeSort()
	{
		setupEscenario7();
		SortingAlgorithms sort= new SortingAlgorithms();
		lista=sort.mergeSort(lista,true);
		assertEquals("La posicion no es la correcta", 1, lista.get(0));
		assertEquals("La posicion no es la correcta", 2, lista.get(1));
		assertEquals("La posicion no es la correcta", 3, lista.get(2));
		

	}
	
	public void testMergeSort2()
	{
		setupEscenario3();
		SortingAlgorithms sort= new SortingAlgorithms();
		List result=sort.mergeSort(lista,true);
		assertEquals("La posicion no es la correcta", 1, result.get(0));
		assertEquals("La posicion no es la correcta", 3, result.get(1));
		assertEquals("La posicion no es la correcta", 4, result.get(2));
		assertEquals("La posicion no es la correcta", 5, result.get(3));
		assertEquals("La posicion no es la correcta", 6, result.get(4));
		assertEquals("La posicion no es la correcta", 7, result.get(5));
		assertEquals("La posicion no es la correcta", 8, result.get(6));
		assertEquals("La posicion no es la correcta", 9, result.get(7));
		assertEquals("La posicion no es la correcta", 10, result.get(8));
		assertEquals("La posicion no es la correcta", 12, result.get(9));
		assertEquals("La posicion no es la correcta", 13, result.get(10));
		assertEquals("La posicion no es la correcta", 15, result.get(11));
		
		System.out.println("\n " +"resultado final ");
		for(int i=0; i<result.size(); i++)
		{
			System.out.println(result.get(i));
			
		}	
	}
	
	public void testAddNode()
	{
		setupEscenario4();
		assertEquals("La posicion no es la correcta", 1, lista.get(0));
		assertEquals("La posicion no es la correcta", 2, lista.get(1));
		assertEquals("La posicion no es la correcta", 3, lista.get(2));
		
	}
	
	public void testAddNode2()
	{
		setupEscenario6();
		Node<Integer> removed= lista.removeFirst();
		int num =removed.getObject();
		assertEquals("La posicion no es la correcta", 1,num  );
		
	}
	
	public void testMerge()
	{
		lista = new List<Integer>();
		
		lista1 = new List<Integer>();
		lista1.add(1);
		lista.add(2);
		lista.add(3);
//		lista1.add(4);
//		lista1.add(6);
//		lista.add(2);
//		lista.add(3);
//		lista.add(7);
	
		
		
		SortingAlgorithms sort= new SortingAlgorithms();
	    List result= sort.merge(lista1,lista);
		System.out.println("info: "+result.get(0) +result.get(1)+result.get(2));
		assertEquals("La posicion no es la correcta", 1, result.get(0));
		assertEquals("La posicion no es la correcta", 2, result.get(1));
		assertEquals("La posicion no es la correcta", 3, result.get(2));
	}
	
	public void testMergeSort3()
	{
	setupEscenario3();
	SortingAlgorithms sort= new SortingAlgorithms();
	List result=sort.mergeSort(lista,false);
	System.out.println("\n " +"resultado final ");
	for(int i=0; i<result.size(); i++)
	{
		System.out.println(result.get(i));
		
	}
	assertEquals("La posicion no es la correcta", 15, result.get(0));
	assertEquals("La posicion no es la correcta", 13, result.get(1));
	assertEquals("La posicion no es la correcta", 12, result.get(2));
	assertEquals("La posicion no es la correcta", 10, result.get(3));
	assertEquals("La posicion no es la correcta", 9, result.get(4));
	assertEquals("La posicion no es la correcta", 8, result.get(5));
	assertEquals("La posicion no es la correcta", 7, result.get(6));
	assertEquals("La posicion no es la correcta", 6, result.get(7));
	assertEquals("La posicion no es la correcta", 5, result.get(8));
	assertEquals("La posicion no es la correcta", 4, result.get(9));
	assertEquals("La posicion no es la correcta", 3, result.get(10));
	assertEquals("La posicion no es la correcta", 1, result.get(11));
}
	
	public void testRemove()
	{
		setupEscenario1();
		lista.remove(4);
		assertEquals("La posicion no es la correcta", 1, lista.get(0));
		assertEquals("La posicion no es la correcta", 2, lista.get(1));
		assertEquals("La posicion no es la correcta", 3, lista.get(2));
		assertEquals("La posicion no es la correcta", 5, lista.get(3));
		System.out.println("Los elementos que quedan en la lista son \n"+lista.get(0) + "\n"+ lista.get(1));
	}
	
	public void testRemove2()
	{
		setupEscenario1();
		lista.remove(1);
		assertEquals("No asigna el primero", 2, lista.getFirst().getObject());
		assertEquals("La posicion no es la correcta", 2, lista.get(0));
		assertEquals("La posicion no es la correcta", 4, lista.get(1));
		assertEquals("La posicion no es la correcta", 3, lista.get(2));
		assertEquals("La posicion no es la correcta", 5, lista.get(3));
		
	}
	
	public void testRemove3()
	{
		setupEscenario1();
		lista.remove(5);
		assertEquals("No asigna el ultimo", 3, lista.getLast().getObject());
		assertEquals("La posicion no es la correcta", 1, lista.get(0));
		assertEquals("La posicion no es la correcta", 2, lista.get(1));
		assertEquals("La posicion no es la correcta", 4, lista.get(2));
		assertEquals("La posicion no es la correcta", 3, lista.get(3));
		
	}
	
	
	

}
