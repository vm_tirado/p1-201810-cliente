package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.List;

public class CompaniaServicios implements Comparable<CompaniaServicios> {
	
	private String nomCompania;
	
	private LinkedList<Servicio> servicios;

	public CompaniaServicios(String nom, List<Servicio> ser)
	{
		nomCompania=nom;
		servicios= new List<Servicio>();
		servicios= ser;
		
	}
	public String getNomCompania() {
		return nomCompania;
	}

	public void setNomCompania(String nomCompania) {
		this.nomCompania = nomCompania;
	}

	public LinkedList<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(LinkedList<Servicio> servicios) {
		this.servicios = servicios;
	}

	@Override
	public int compareTo(CompaniaServicios o) {
		return servicios.size()-o.servicios.size();
	}
	
	

}
