package model.vo;

import java.util.Date;

import model.data_structures.LinkedList;
import model.data_structures.List;

public class FechaServicios implements Comparable<FechaServicios>{
	

	
	private Date fecha;
	private LinkedList<Servicio> serviciosAsociados;
	private int numServicios;
	
	public FechaServicios()
	{
		serviciosAsociados= new List<Servicio>();
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public LinkedList<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(LinkedList<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public int getNumServicios() {
		return numServicios;
	}
	public void setNumServicios(int numServicios) {
		this.numServicios = numServicios;
	}
	
	public void addService(Servicio ser)
	{
		serviciosAsociados.add(ser);
	}
	@Override
	public int compareTo(FechaServicios o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
