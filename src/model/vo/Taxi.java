package model.vo;

import model.data_structures.List;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	
	private List<Servicio> serviciosInscritos;
	
	String taxi_id;
	
	String company;
	
	
	private List<Servicio> serviciosInscritosFecha;

	
	public Taxi(String pCompany, String pTaxi_Id)
	{
		serviciosInscritos = new List<Servicio>();
		serviciosInscritosFecha = new List<Servicio>();
		taxi_id=pTaxi_Id;
		company= pCompany;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}
	
	
	public List<Servicio> getServiciosInscritos() {
		return serviciosInscritos;
	}

	
	public List<Servicio> getServiciosInscritosFecha() {
		return serviciosInscritosFecha;
	}

	public void add1(Servicio T)
	{
		serviciosInscritosFecha.add(T);
	}
	
	
	
	public double dardistanciaRecorrida()
	{
		double total=0;
		
		for(Servicio servicito: serviciosInscritos)
		{
			total+=servicito.getTripMiles();
			
		}
		
		return total;
		
	}
	
	
	public double darrentabilidad()
	
	{
		
		return darplataGanada()/dardistanciaRecorrida();
		
	}

	
	public double darplataGanada()
	{
		double total=0;
		
		for(Servicio servicito: serviciosInscritos)
		{
			total+=servicito.getTripTotal();
			
		}
		
		return total;
		
	}
	

	public void setServiciosInscritosF(List<Servicio> pserviciosInscritos) {
		this.serviciosInscritos = pserviciosInscritos;
	}
	
	public void add(Servicio T)
	{
		serviciosInscritos.add(T);
	}


	public void setServiciosInscritos(List<Servicio> pserviciosInscritos) {
		this.serviciosInscritos = pserviciosInscritos;
	}

	
	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}


	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return o.getTaxiId().compareTo(taxi_id);
	}	
}
