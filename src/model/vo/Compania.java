package model.vo;

import model.data_structures.List;

public class Compania implements Comparable<Compania> {
	
	
	private List<Taxi> taxisInscritos;
	
	String company;
	

	
	public Compania(String pNombre)
	{
		
		company=pNombre;
		taxisInscritos= new List<Taxi>();
		
	}
	
	
	public String getNombre() {
		return company;
	}

	public void setNombre(String nombre) {
		this.company = nombre;
	}

	public List<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}
	
	public void add(Taxi T)
	{
		taxisInscritos.add(T);
	}

	public void setTaxisInscritos(List<Taxi> taxisInscritos) {
		this.taxisInscritos = taxisInscritos;
	}

	@Override
	public int compareTo(Compania o) {
		// TODO Auto-generated method stub
		return o.getNombre().compareTo(company);
	}
	
	
	

}
