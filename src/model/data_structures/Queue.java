package model.data_structures;

import java.util.Iterator;




public class Queue<T extends Comparable <T>> implements Iterable<T>, IQueue<T> {

	protected Node<T> first;
	protected Node<T> last;
	protected int size;

	public Queue()
	{
		first= null;
		last=null;
		size=0;
	}

	@Override
	public void enqueue(T obj) {
		if(first!=null)
		{

			Node<T> n= new Node<T>(obj,null,last);
			last.changeNext(n);
			last=last.next();
			size++;

		}
		else
		{
			Node<T> n = new Node<T>(obj,null,null);
			first=n;
			last=first;
			size++;
		}

	}

	@Override
	public T dequeue()
	{
		T deleted=null;
		if (first!=null)
		{
			if (first.next()!=null){
				deleted=first.getObject();
				Node<T> newFirst= first.next();
				newFirst.changePrevious(null);
				first=newFirst; 
				size--;
			}
			else 
			{
				deleted=first.getObject();
				first=null;
				last=first;
				size--;
			}
		}
		return deleted;
	}



	public boolean isEmpty()
	{
		return size==0?true:false;
	}

	public T get(T obj) {
		T object=null;
		Node<T> curr=first;
		while(curr!=null && object==null)
		{
			if (curr.getObject().compareTo(obj)==0)
			{
				object=curr.getObject();
			}
			curr=curr.next();
		}
		return object;
	}

	public int size() {

		return size;
	}

	public T get(int position) 
	
	
	{

		Node<T> curr=first;

		int j=0;
		while(curr!=null && j!= position)
		{
			j++;
			curr=curr.next();
		}
		return curr!=null? curr.getObject():null;

	}

	public  Node<T> getFirst()
	{
		return first;
	}

	public Node<T> getLast() {
		return last;
	}

	public Node<T> next() {
		Node<T> node=null;
		if(last.next().getObject()!=null)
		{
			last=last.next();
			node=last;
		}
		return node;
	}

	public boolean contains(T nodo)
	{		
		for(T nodito: this)
		{

			if(nodito!=null &&nodito.compareTo(nodo)==0)
			{
				return true;
			}
		}
		return false;		
	}



	public Iterator<T> iterator() {

		return new QueueItrator();
	}
	private class QueueItrator implements Iterator<T>
	{
		private Node<T> curr= first;
		@Override
		public boolean hasNext() {
			return curr!=null;
		}

		@Override
		public T next() {
			T next=curr.getObject();
			curr=curr.next();
			return next;

		}
	}
}