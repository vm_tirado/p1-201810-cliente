package model.logic;

import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Exchanger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.List;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.SortingAlgorithms;
import model.data_structures.Stack;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	private Queue<Servicio> colaServicios; 
	private Queue<Taxi> colaTaxis; 
	private List<Compania> listacompanias;
	private List<Taxi> taxisSinCompania;


	boolean bool = false;


	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		colaServicios = new Queue<Servicio>();
		colaTaxis = new Queue<Taxi>();
		listacompanias = new List<Compania>();
		taxisSinCompania = new List<Taxi>();



		long time=System.currentTimeMillis();

		if (!bool)
		{

			try
			{
				Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
				Servicio[] item= gson.fromJson(new FileReader(direccionJson), Servicio[].class);

				for(int i=0; i<item.length;i++)
				{

					colaServicios.enqueue(item[i]);


				}





				for(int i=0; i<colaServicios.size();i++)
				{
					Taxi taxito = new Taxi(item[i].getCompany(),item[i].getTaxiid());

					if(!colaTaxis.contains(taxito))

					{
						colaTaxis.enqueue(taxito);

					}

				}
				
				
				

				for(Taxi taxito : colaTaxis)
				{

					if(taxito.getCompany()==null)
					{
						
						taxisSinCompania.add(taxito);
						
						
					}
							
				}
				

	
			
				for(int i=0; i<colaTaxis.size();i++)
				{
					Compania companita = new Compania(colaTaxis.get(i).getCompany());

					if(companita.getNombre()!=null&&!listacompanias.contains(companita))
					{

						listacompanias.add(companita);
					}
				}

				bool=true;
				
				
				
				
				for(Compania companita: listacompanias)
				{
				
				for(Taxi taxito : colaTaxis )
					
				{
					
				if(taxito.getCompany()!=null&&taxito.getCompany().equalsIgnoreCase(companita.getNombre()))
				{
					
					companita.add(taxito);
					
					
				}
				
				
					
				}
				
				
				}
				
				for(Servicio servicito: colaServicios)
				{
				
				for(Taxi taxito : colaTaxis )
					
				{
					
				if(taxito.getTaxiId()!=null&&taxito.getTaxiId().equalsIgnoreCase(servicito.getTaxiid()))
				{
					

					taxito.add(servicito);
					
					
				}
				
				
					
				}
				
				
				}
				
				
			}
			
	
			catch (Exception e)
			{
				e.printStackTrace();
				System.out.println("Hubo un error al cargar el archivo");
			}


			System.out.println(colaServicios.size());
			System.out.println(colaTaxis.size());
			System.out.println(listacompanias.size());
			System.out.println("Hay "+taxisSinCompania.size()+" Taxis independientes");
			System.out.println(listacompanias.get(0).getNombre()+" tiene "+listacompanias.get(0).getTaxisInscritos().size()+" Taxis");
			System.out.println("Inside loadServices with " + direccionJson);
			System.out.println("tiempo "+(System.currentTimeMillis()-time));
		}

		else

		{
			System.err.println("El archivo solamente puede ser cargado una vez en el programa");


		}

		System.gc();
		return false;
	}

	@Override //1A
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{

		Queue<Servicio> colaServiciosFecha = new Queue<Servicio>();
		List<Servicio> listaServiciosFecha = new List<Servicio>();
		List<Servicio> listaServiciosFechaOrden = new List<Servicio>();

		

		// TODO Auto-generated method stub
		for(int i=0;i<colaServicios.size(); i++)

		{			

			if(colaServicios.get(i)!=null)
			{

				if(colaServicios.get(i).isInRange(rango))
				{
					colaServiciosFecha.enqueue(colaServicios.get(i));

				}
			}

		}

		for(Servicio servicito: colaServiciosFecha)
		{
			listaServiciosFecha.add(servicito);
			
		}
		
		SortingAlgorithms<Servicio> mergeador = new SortingAlgorithms<Servicio>();
		
	listaServiciosFechaOrden=mergeador.mergeSort(listaServiciosFecha, true);
		
		
		
		for(Servicio unServicio: listaServiciosFechaOrden)
		{
			System.out.println(unServicio.getDropoff_start_timestamp());
		}

		return colaServiciosFecha;
	}

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		// TODO Auto-generated method stub
		
		Compania encontrada = new Compania("No se encontr� ninguna compa��a con dicho nombre");
		Taxi taxialfa = null;
		
		
		for(Compania companita: listacompanias)
		{
			if(companita.getNombre().equalsIgnoreCase(company))
				
			{
				encontrada = companita;
				
			}
			
			
		}
		
		
		for(Taxi taxito: encontrada.getTaxisInscritos())
		{
			for(Servicio servicito: taxito.getServiciosInscritos())
			{
				if(servicito.isInRange(rango))
				{
					taxito.add1(servicito);
					
				}
				
				
			}
			
			
		}
		
	

		for(Taxi taxito: encontrada.getTaxisInscritos())
		{
			taxialfa = encontrada.getTaxisInscritos().get(0);
			
			if(taxito.getServiciosInscritosFecha().size()>taxialfa.getServiciosInscritosFecha().size())
				
			{
				taxialfa= taxito;
				
			}
			
			
			
		}
		
		
		System.out.println(taxialfa.getTaxiId());
		System.out.println(taxialfa.getCompany());
		return taxialfa;
	}

	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		String compania= "";
		Taxi encontrado= null;
		double plataganada=0;
		double distanciaRecorrida=0;
		double tiempoTotal=0;
		
		List<Servicio> serviciosRango = new List<Servicio>();

	
	for(Taxi taxito: colaTaxis)
	{
		
		if(taxito.getTaxiId().equalsIgnoreCase(id))
		{
		encontrado=taxito;
		}
	}
	

	
	
	for(Servicio servicito: encontrado.getServiciosInscritos())
	{
		if(servicito.isInRange(rango))
		{
			serviciosRango.add(servicito);
		}
		
		
	}
	
	//Almacena la informaci�n de cada uno de los taxis. 
	
	for(Servicio otroServicio: serviciosRango)
	{
		
		plataganada+=otroServicio.getTripTotal();
		distanciaRecorrida+=otroServicio.getTripMiles();
		tiempoTotal+=otroServicio.getTripSeconds();
		
		
	}
	
	if(encontrado.getCompany()!=null)
	{

		compania=encontrado.getCompany();
		
	}
	
	else
	{
		compania="No tengo compa��a";
		
	}

		System.out.println("Compa��a: "+compania+" Id: "+id+" Dinero ganado: "+ plataganada + " Servicios prestados: " +serviciosRango.size()+" Distancia recorrida: "+distanciaRecorrida+" millas"+" Tiempo de viaje: "+tiempoTotal+" segundos");
		

		return null;
	}

	@Override //4A
	public List<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		
		List<RangoDistancia> listaRangos = new List<RangoDistancia>();
		List<Servicio> serviciosRango = new List<Servicio>();

		Date fechaI= new Date();
		Date fechaF= new Date();
		double rangoMaximo = 0;
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		
		try {
			fechaI = formato.parse(fecha+'T'+horaInicial);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
		fechaF = formato.parse(fecha+'T'+horaFinal);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		RangoFechaHora ranguito = new RangoFechaHora(fechaI, fechaF); 
		
		
		for(Servicio servicito: colaServicios)
		{
			if(servicito.isInRange(ranguito))
			{
				serviciosRango.add(servicito);
				
			}	
			
		}
		
		for(Servicio otroServicio: serviciosRango)
		{
			if(otroServicio.getTripMiles()>rangoMaximo)
			{
				rangoMaximo = otroServicio.getTripMiles();
				
			}
			
			
		}
		
		
		
		for(int i=1; i<rangoMaximo;i++)
		{			
			
			RangoDistancia temp= new RangoDistancia(i, i-1);

			listaRangos.add(temp);
			
		}
		
		
		for(RangoDistancia rango: listaRangos)
		{
			for(Servicio unServicio:serviciosRango)
			{
				if(unServicio.isInRangeM(rango))
				{
					rango.add(unServicio);
					
				}
				
			}
			
		}
	
		
		
		for(int i=0; i<listaRangos.size(); i++)
		{
			System.out.println("En el rango de "+listaRangos.get(i).getLimineInferior()+" a "+listaRangos.get(i).getLimiteSuperior()+" hay " + listaRangos.get(i).getServiciosEnRango().size()+" servicios.");
			
		}
		
		System.out.println("Habr�n: "+ listaRangos.size()+" rangos de distancia para "+serviciosRango.size()+" servicios.");
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override //1B
	public LinkedList<Compania> darCompaniasTaxisInscritos() 
	{
		long startTime = System.nanoTime();
		for (int i =0; i<colaTaxis.size() ; i++)
		{
			if (colaTaxis.get(i)!=null)
			{
				for (int j=0; j<listacompanias.size(); j++)
				{

					if(listacompanias.get(j)!=null)
					{
						if (  colaTaxis.get(i).equals(listacompanias.get(j).getNombre()))
						{
							listacompanias.get(j).add(colaTaxis.get(i));
						}
					}

				}
			}
			SortingAlgorithms<Compania> merge= new SortingAlgorithms<Compania>();
			listacompanias=merge.mergeSort(listacompanias , false);
		}
		long endTime = System.nanoTime();
		long duration = (endTime - startTime)/(1000000);
		System.out.println("Tiempo en cargar: " + duration + " milisegundos");
		return listacompanias ;

	}


	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		List<Servicio> listaServicios= new List<Servicio>();
		for(int i=0;i<colaServicios.size(); i++)
		{			
			if(colaServicios.get(i)!=null)
			{
				if(colaServicios.get(i).isInRange(rango) )
				{
					listaServicios.add(colaServicios.get(i));

				}
			}

		}
		double mayor=0;
		Taxi tax = null;
		for (int i=0; i<colaTaxis.size(); i++)
		{
			double cont=0;
			for (int j=0; j<listaServicios.size(); j++)
			{
				if (listaServicios.get(j).getTaxiid().equals(colaTaxis.get(i).getTaxiId()))
				{
					cont+=listaServicios.get(j).getFare();
				}

			}
			if (cont>mayor)
			{
				mayor=cont;
				tax=colaTaxis.get(i);
			}
		}
		return tax;

	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		long startTime = System.nanoTime();
		List<Servicio> serviciosEnRango= new List<Servicio>();
		int cont=0;
		ServiciosValorPagado[] info= new ServiciosValorPagado[3];
		List<Servicio> listaMismaZona= new List<Servicio> ();
		double pMismaZona=0;
		List<Servicio> listaEmpiezanEnZona= new List<Servicio>();
		double pEmpiezanEnZona=0;
		List<Servicio> listaTerminanEnZona= new List<Servicio>();
		double pTerminanEnZona=0;
		for(int i=0;i<colaServicios.size(); i++)
		{			
			if(colaServicios.get(i)!=null)
			{
				if(colaServicios.get(i).getDropoff_start_timestamp().before(rango.getFechaFinal())&&colaServicios.get(i).getDropoff_end_timestamp().after(rango.getFechaInicial()) )
				{
					serviciosEnRango.add(colaServicios.get(i));
					cont ++;
				}
			}
		}
		for(Servicio ser:serviciosEnRango)
		{
			if(ser.getDropoff_community_area()==Integer.parseInt(idZona) && ser.getPickup_community_area()==Integer.parseInt(idZona))
			{
				listaMismaZona.add(ser);
				pMismaZona+=ser.getFare();
			}
			else if(ser.getDropoff_community_area()==Integer.parseInt(idZona))
			{
				listaTerminanEnZona.add(ser);
				pTerminanEnZona+=ser.getFare();
			}
			else 
			{
				listaEmpiezanEnZona.add(ser);
				pEmpiezanEnZona+=ser.getFare();
			}
		} 
		System.out.println(cont +"\n");
		//Empiezan en Zona
		ServiciosValorPagado empiezanEnZona= new ServiciosValorPagado();
		empiezanEnZona.setServiciosAsociados(listaEmpiezanEnZona);
		empiezanEnZona.setValorAcumulado(pEmpiezanEnZona);
		info[0]= empiezanEnZona;

		// Terminan en Zona
		ServiciosValorPagado terminanEnZona = new ServiciosValorPagado();
		terminanEnZona.setServiciosAsociados(listaTerminanEnZona);
		terminanEnZona.setValorAcumulado(pTerminanEnZona);
		info[1]= terminanEnZona;

		//Misma Zona
		ServiciosValorPagado mismaZona= new ServiciosValorPagado();
		mismaZona.setServiciosAsociados(listaMismaZona);
		mismaZona.setValorAcumulado(pMismaZona);
		info[2]= mismaZona;

		long endTime = System.nanoTime();
		long duration = (endTime - startTime)/(1000000);
		System.out.println("Tiempo en cargar: " + duration + " milisegundos");
		return info;
	}


	@Override //4B
	public LinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		LinkedList<FechaServicios> FechaS= new List<FechaServicios>();
		//Lista de los servicios que estan en rango
		List<Servicio> serRango= new List<Servicio>();
		// Se crean las listas de los servicios en rango 
		// Se crean las fechasServicios en rango 
		for(Servicio s: colaServicios)
		{
			if(s!=null && s.isInRange(rango))
			{
				serRango.add(s);

				FechaServicios fech= new FechaServicios();
				fech.setFecha(s.getDropoff_start_timestamp());
				fech.addService(s);
				FechaS.add(fech);
				continue;
			}
			else if(serRango.size()!=0)
			{
				if (s==null ) continue;
				break;
			}

		}

		//Listas de Zonas
		List<ZonaServicios> zonaS= new List<ZonaServicios>();
		for (int i=0; i<serRango.size(); i++)
		{
			Servicio ser= serRango.get(i);
			if (ser!=null)
			{
				//Crea la zona 
				ZonaServicios zona = new ZonaServicios();
				zona.setIdZona(""+ser.getPickup_community_area());
				//Si la lista de zonas no la contiene la a�ade 
				if(!zonaS.contains(zona))
				{
					zonaS.add(zona);
				}
			}
		}
		//Recorre la lista de zonas
		for(ZonaServicios z: zonaS)
		{
			//Crea una fecha servicio para cada zona 
			List<FechaServicios> fechaServicios= new List<FechaServicios>();
			if (z!=null)
			{
				
				//recorre todos los servicios que estan en rango 
				for (int i=0; i<serRango.size(); i++)
				{
					Servicio ser= serRango.get(i);
					//Si el servicio actual es de la misma Zona que la Zona actual 
					//Crea una fecha con la fecha del servicio 
					//Si la lista de fechas de la zona no la contiene la a�ade 
					if(ser!=null && ser.getPickup_community_area()== Integer.parseInt(z.getIdZona()))
					{
						FechaServicios f = new FechaServicios();
						f.setFecha(ser.getDropoff_start_timestamp());
						if(!fechaServicios.contains(f))
						{
							fechaServicios.add(f);
						}
						//Le a�ade a la lista de servicios el servicio actual 
						fechaServicios.get(f).addService(ser);
						// Le quita a la lista de servicios en rango el servicio que 
						//Ya se a�adio para que este no se a�ada dos veces 
						serRango.remove(ser);
					}
					
				}
				
			}
			//Le a�ade a la zona actual las fechas Servicios 
			z.setFechasServicios(fechaServicios);
		}
		
		return zonaS; 
	}

	@Override //2C
	public LinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		int numTop= n;
		List<CompaniaServicios> top= new List<CompaniaServicios>();
		List<Servicio> listaServicios= new List<Servicio>();
		List<Servicio> serviciosCompania= new List<Servicio>();
		List<CompaniaServicios> listaCompania= new List<CompaniaServicios>();
		for (Servicio ser:colaServicios)
		{
			if(ser.isInRange(rango))
			{
				listaServicios.add(ser);
			}
		}

		for(Servicio ser: listaServicios)
		{
			if(ser!=null)
			{
				for (int i=0 ;i< listaServicios.size(); i++)
				{
					if(listaServicios.get(i)!=null)
					{
						if(ser.getCompany().equals(listaServicios.get(i).getCompany()))
						{
							serviciosCompania.add(listaServicios.get(i));
						}
					}
				}
			}
			CompaniaServicios comp= new CompaniaServicios(ser.getCompany(),serviciosCompania);
			if (!listaCompania.contains(comp))
			{
				listaCompania.add(comp);
			}
		}
		SortingAlgorithms sort = new SortingAlgorithms();
		listaCompania= sort.mergeSort(listaCompania, false);

		if(listaCompania.size()<n)
		{
			n=listaCompania.size();
		}
		for (int i=0; i<n; i++)
		{
			top.add(listaCompania.get(n));
		}

		return  top;
	}

	@Override //3C
	public List<Compania> taxisMasRentables()
	{
		// TODO Auto-generated method stub
		
		List<Compania> listarentable = new List<Compania>(); 

		// Busca el taxi m�s rentable de cada compa��a
		
		for(Compania companita: listacompanias)
		{
			
			Taxi masRentable= companita.getTaxisInscritos().get(0);

			for(Taxi taxito: companita.getTaxisInscritos())
			{
				if(taxito.darrentabilidad()>masRentable.darrentabilidad())
				{
					
					masRentable=taxito;
					
				}
				
				
			}
			
			listarentable.add(companita);
			companita.add(masRentable);
			
		}


		for(Compania company: listarentable)
		{
			System.out.println(company.getNombre()+": "+company.getTaxisInscritos().getFirst().getObject().getTaxiId());
		}
		return listarentable;
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		// TODO Auto-generated method stub
		
SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		

	Stack<Servicio> pilaServicio = new Stack<Servicio>();
	List<Servicio> listaTemp = new List<Servicio>();
	List<Servicio> listaOrdenada = new List<Servicio>();
	Date fechaIn = null ;
	Date fechaFn = null ;
	double distancia=0;
	double plataganada=0;
	int tiempo=0;
	

	Date fechaI= new Date();
	Date fechaF= new Date();
	Taxi encontrado = null;

		try {
			fechaI = formato.parse(fecha+'T'+horaInicial);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
		fechaF = formato.parse(fecha+'T'+horaFinal);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RangoFechaHora ranguito = new RangoFechaHora(fechaI, fechaF); 
		
		
		for (Taxi taxito: colaTaxis)
		{
			if(taxito.getTaxiId().equalsIgnoreCase(taxiId))
			{
				encontrado = taxito;
				
			}
			
		}
		
		
		for(Servicio servicito: encontrado.getServiciosInscritos())
		{
			
			if(servicito.isInRange(ranguito))
			{

				listaTemp.add(servicito);
			}
			
		}

		
		SortingAlgorithms<Servicio> mergeador = new SortingAlgorithms<Servicio>();
		listaOrdenada=mergeador.mergeSort(listaTemp, true);

		
		
		
		for(Servicio otroServicio: listaOrdenada)
		{
			
			pilaServicio.push(otroServicio);
			plataganada+=otroServicio.getTripTotal();
			distancia+=otroServicio.getTripMiles();
			tiempo+=otroServicio.getTripSeconds();
					
		}
		
		
		if(distancia>=10)
		{
			fechaIn= listaOrdenada.getFirst().getObject().getDropoff_start_timestamp();
			fechaFn= listaOrdenada.getLast().getObject().getDropoff_end_timestamp();
			pilaServicio = null;
			pilaServicio = new Stack<Servicio>();
			
			Servicio comprimido = new Servicio();
			comprimido.setCompany(encontrado.getCompany());
			comprimido.setTaxi_id(encontrado.getTaxiId());
			comprimido.setTrip_total(plataganada);
			comprimido.setTrip_start_timestamp(fechaIn);
			comprimido.setTrip_end_timestamp(fechaFn);
			comprimido.setTrip_seconds(tiempo);
			comprimido.setTrip_miles(distancia);
			
			pilaServicio.push(comprimido);
			System.out.println("El servicio fue resumido");
		}
		
		else
		{
			
			System.out.println("Nada que resumir");
			System.out.println(listaOrdenada.size()+" servicios");

		}

		
		for(Servicio serviceX: pilaServicio)
		{
			
			System.out.println(serviceX.getTripMiles());
			
		}
		
//		System.out.println("Compa��a: "+encontrado.getCompany()+" Id: "+encontrado.getTaxiId()+" Dinero ganado: "+ plataganada + " Servicios prestados: " +listaOrdenada.size()+" Distancia recorrida: "+distancia+" millas"+" Tiempo de viaje: "+tiempo+" segundos");
//
//		System.out.println("El Taxi ha recorrido: "+distancia+" millas en el rango establecido");
		
		
		return pilaServicio;
	}


//	//Algoritmos de ordenamiento 
//
//	public List ShellSort(List pLista)
//	{
//		int N= pLista.size();
//		int h=1;
//		while (h<N/3) h=3*h+1;
//		while (h>=1)
//		{
//			for(int i=h ;i<N;i++)
//			{
//				for (int j=i; j>h&& pLista.get(j).compareTo(pLista.get(j-h))==-1;j-=h)
//				{
//					pLista.exchange(j, j-h);
//				}
//				h=h/3;
//			}
//		}
//		return pLista;
//	}
//
//
//
//	public List mergesort(List lista)
//	{
//		int low=0;
//		int high= lista.size()-1;
//		List left;
//		List right = new List ();
//		List result= new List();
//		int middle= lista.size()/2;
//		Node Nmiddle=(Node) lista.get(middle);
//		Node NafterMiddle= (Node) lista.get(middle+1);
//		if(lista.size()<=1)
//		{
//			return lista;
//		}
//		else 
//		{
//			right=new List();
//			right.setFirst(NafterMiddle);
//			right.setLast(lista.getLast());
//			Nmiddle.changeNext(null);
//			NafterMiddle.changePrevious(null);
//			left=lista;
//			left.setLast(Nmiddle);
//			right.setSize(lista.size()-middle);
//			left.setSize(middle);
//			left=mergesort(left);
//			right=mergesort(right);
//			Node n=merge(left,right);
//			result.setFirst(n);
//			return result;
//		}
//	}
//
//
//	public Node merge(List left, List right)
//	{
//		List result = new List();
//		while(left.size()>0 && right.size()>0)
//		{
//			if (left.getFirst().getObject().compareTo(right.getFirst().getObject())==-1||left.getFirst().getObject().compareTo(right.getFirst().getObject())==0)
//			{
//				result.add(left.removeFirst());
//			}
//			else
//				result.add(right.removeFirst());
//		}
//		return result.getFirst();
//	}
}
